FROM debian

ADD https://github.com/just-containers/s6-overlay/releases/download/v1.21.8.0/s6-overlay-amd64.tar.gz /tmp/
#ADD ./s6-overlay-amd64.tar.gz /tmp/

RUN tar xzf /tmp/s6-overlay-amd64.tar.gz -C /

RUN apt update && apt install python python-pip python-virtualenv asterisk -y && apt clean

ADD ./permissions       /etc/fix-attrs.d/permissions

ADD ./asterisk-init.sh  /etc/cont-init.d/asterisk.sh
ADD ./sip-init.sh       /etc/cont-init.d/sip.sh

ADD ./services/         /etc/services.d/

ADD ./asterisk_config/  /etc/asterisk/
ADD ./sip_daemon/       /var/sip_daemon/

RUN python -m pip install -r '/var/sip_daemon/requirements.txt' 

VOLUME /var/spool/asterisk/recording/
VOLUME /var/lib/asterisk/sounds
VOLUME /gapis.json

CMD /init