import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    APP_NAME = "coroline"

    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'
       
    ARI_URI = 'http://localhost:8088/'
    ARI_USER = 'asterisk'
    ARI_PASSWORD = 'asterisk'
    ARI_APP = 'app'

    EMAIL_SENDER_EMAIL = "entraide@ouest-lausannois.ch"

    MAIL_HOST = "localhost"
    MAIL_PORT = "1025"

    SOUND_BASE_DIR = "/var/lib/asterisk/sounds"
    SOUND_RECORD_DIR = "/var/spool/asterisk/recording"
    RECORD_EXTENSION = ".wav"

    API_BASE_URL = os.environ.get('API_BASE_URL') or "http://localhost:3000"
    API_KEY = os.environ.get('API_KEY') or "toto"

    LANG = "fr-FR"