#!/usr/bin/env python

"""Example demonstrating using the returned object from an API call.
This app plays demo-contrats on any channel sent to Stasis(hello). DTMF keys
are used to control the playback.
"""

#
# Copyright (c) 2020, Teo Goddet
#

import ari
import sys, logging, os, time, uuid

from config import Config as config

from phone_app.models import *

from phone_app.event import Event
from phone_app.statemachine import StateMachine

import phone_app.states as states
states.import_submodules("phone_app.states")

client = ari.connect(config.ARI_URI, config.ARI_USER, config.ARI_PASSWORD)

han = logging.StreamHandler()
logger = logging.getLogger('ari')
logger.addHandler(han)

class Call(object):
    #some call object utility
    def __init__(self, client, channel):
        self.client = client
        self.channel = channel

        self.bridge = self.client.bridges.create()
        self.bridge.addChannel(channel=self.channel.id)

        self.record = None

        self.requete = Requete()

        self.playback = None
        self.recording = None


        self.number = channel.json["caller"]["number"]

        self.setup_state_machine()
 
    def setup_state_machine(self):
        hungup_state = states.hungup.State(self)
        recording_state = states.recording.State(self)
        ending_state = states.ending.State(self)
        welcome_state = states.welcome.State(self)
        comment_state = states.comment.State(self)
 
        self.state_machine = StateMachine()
        self.state_machine.add_transition(welcome_state, Event.DTMF_1 ,
                                          recording_state)
        self.state_machine.add_transition(welcome_state, Event.DTMF_2,
                                          recording_state)    
        self.state_machine.add_transition(welcome_state, Event.DTMF_3 ,
                                          recording_state)
        self.state_machine.add_transition(welcome_state, Event.DTMF_5,
                                          ending_state)
        self.state_machine.add_transition(welcome_state, Event.TO_MANY_RESTART,
                                          ending_state)                                         
        self.state_machine.add_transition(welcome_state, Event.HANGUP,
                                          hungup_state)

        self.state_machine.add_transition(recording_state, Event.DTMF_0,
                                          comment_state)
        self.state_machine.add_transition(recording_state, Event.DTMF_1,
                                          comment_state)
        self.state_machine.add_transition(recording_state, Event.HANGUP,
                                          hungup_state)

        self.state_machine.add_transition(comment_state, Event.HANGUP,
                                          hungup_state)
        self.state_machine.add_transition(comment_state, Event.STATE_END,
                                          ending_state)

        self.state_machine.add_transition(ending_state, Event.HANGUP,
                                          hungup_state)

        self.state_machine.start(welcome_state)

    def play(self, name):
        self.playback = self.bridge.playWithId(playbackId = str(uuid.uuid4()), media="sound:{0}".format(os.path.join(config.SOUND_BASE_DIR, name)), lang=config.LANG)

    
def on_start(channel_obj, event):
    """Callback for StasisStart events.
    On new channels, answer, play demo-congrats, and register a DTMF listener.
    :param channel: Channel DTMF was received from.
    :param event: Event.
    """
    channel = channel_obj.get('channel')
    channel.answer()
    Call(client, channel)


client.on_channel_event('StasisStart', on_start)

# Run the WebSocket
client.run(apps=config.ARI_APP)
