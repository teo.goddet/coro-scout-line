# -*- coding: utf-8 -*-

Message = {
	"BIENVENUE" : """
	Bonjour ! En raison des circonstances actuelle, 
	les scouts de l'Ouest vous propose de l'entre aide 
	pour les personnes à risque dans les communes de Renens, 
	Chavannes, Ecublan et Crissiez. 
	Une fois l'appel terminer, des bénévoles vous recontacterons 
	pour fixer les derniers détails.
	Comment pouvons nous vous aider ? 
	Pour des courses taper 1. 
	Pour aller chercher des médicaments en pharmacie taper 2.
	 Pour des opérations à effectuer à la poste , taper 3. 
	 Pour tout autre demande taper 4.""",
	"LECTURECOORDONNEE" : "Nous avons besoin de connaître votre nom et votre adresse afin que les bénévoles puissent ce rendre chez vous. Selon le numéro de téléphone fixe que vous utilisé, vous êtes {{0}} et habiter à {{1}} {{2}}. Si c'est correct, taper 1. Sinon taper 0.",
	"CHANGEMENTCOORDONNEE" : "Ce n'est pas grave. Merci de prononcer vos prénom et nom, puis votre adresse.",
	"DIVERS" : "Si vous avez d'autre remarque à nous comuniqué, laisser nous un message.",
	"BCPG" : "Nous vous remercions pour votre demande. Les bénévoles vous recontacterons d'ici 2 jours pour fixés les derniers détails. Remplir ce formulaire plusieurs fois pour la même demande n'est pas utile. Les scouts de l'Ouest vous font un Bon Check de Pied Gauche.",
}