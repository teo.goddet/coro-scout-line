import os

from config import Config as config
from messages import Message
from phone_app.tts import tts


def buildMessage () :
	for nomMessage, toRead in Message.items():
		tts(toRead, os.path.join(config.SOUND_BASE_DIR, nomMessage + ".sln16"))


buildMessage()