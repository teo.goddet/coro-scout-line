 # -*- coding: utf-8 -*-

import os, time, uuid
import threading
import config

from config import Config as config

from ..event import Event
from base import BaseState

from ..search_ch_api import InfosFromNumber

from ..models import *

from ..tts import tts
from messages import Message

class State(BaseState):
    state_name = "recording"

    def enter(self):
        print "enter recording state"

        self.hangup_event = self.call.channel.on_event('ChannelHangupRequest',
                self.on_hangup)

        record_ref = os.path.join(self.call.number, str(time.time()))

        self.call.record = self.call.bridge.record(name=record_ref,format='wav',beep=True,ifExists='overwrite')

        self.call.requete = Requete()

        self.call.requete.enregistrement = Enregistrement(record_ref)
        
        (nom, addresse) = InfosFromNumber(self.call.number) 

        verify = True

        if nom == None:
           nom = "Unknow"
           verify = False

        if addresse == None:    
           verify = False
           addresse = Addresse("Unknow", "Unknow", "Unknow")

        self.call.requete.personne = Personne(self.call.number, nom, addresse)


        if verify:
            try: 
                self.call.playback.stop()
            except Exception as error:
                print("error : ", error)

            tts_uid = str(uuid.uuid4())
            tts( Message["LECTURECOORDONNEE"].format(nom, addresse.rue, addresse.commune)
               , os.path.join(config.SOUND_BASE_DIR, "temp", "{0}.sln16".format(tts_uid)))

            self.call.playback = self.call.bridge.playWithId(playbackId = str(uuid.uuid4()), 
                                                             media="sound:{0}".format(os.path.join(config.SOUND_BASE_DIR, "temp", tts_uid)), 
                                                             lang=config.LANG) 
            self.dtmf_event = self.call.channel.on_event('ChannelDtmfReceived',
                                                     self.on_dtmf)
            #etes vous bien madame xxx habitant au xxxx ? si oui tapez 1, sinon tapez 0
            
            self.timer = threading.Timer(60, self.play_promt_again, [tts_uid]) 
            self.timer.start()     

        else:
            self.go_dtmf_0()


        #self.silence_event = self.call.channel.on_event('ChannelTalkingFinished', self.on_talk_finish)
    def on_dtmf(self, channel, event):
        digit = event.get('digit')

        if digit == '0':           
            try:
                self.call.playback.stop()
            except Exception as error:
                print("error : ", error)
        if not digit in ['0', '1', '9']:
            return

        self.cleanup()
        self.call.state_machine.change_state(digit)


    def play_promt_again(self, tts_uid):
        try:
            self.call.playback.stop()
        except Exception as error:
            print("error : ", error)
        self.call.playback = self.call.bridge.playWithId(playbackId = str(uuid.uuid4()), 
                                                         media="sound:{0}".format(os.path.join(config.SOUND_BASE_DIR, "temp", tts_uid)), 
                                                         lang=config.LANG) 

        self.timer = threading.Timer(60, self.go_dtmf_0) 
        self.timer.start()   

    def go_dtmf_0(self):
        try:
            self.call.playback.stop()
        except Exception as error:
            print("error : ", error)
        self.cleanup()
        self.call.state_machine.change_state('0')


