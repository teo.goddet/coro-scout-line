import os, time, uuid
import threading


from phone_app.event import Event

class BaseState(object):
    def __init__(self, call):
        self.call = call
        self.hangup_event = None
        self.dtmf_event = None
        self.silence_event = None
        self.sound_event = None
        self.recording = None
        self.timer = None
 

    def cleanup(self):
        print "Cleaning up event handlers"
        try: 
            self.call.playback.stop()
        except Exception as error:
            print("error : ", error)
        
        if not self.timer == None:
            self.timer.cancel()    

        if not self.dtmf_event == None:
            self.dtmf_event.close()
        if not self.hangup_event == None:
            self.hangup_event.close()
        if not self.sound_event == None:
            self.sound_event.close()
 
    def on_hangup(self, channel, event):
        self.cleanup()
        self.call.state_machine.change_state(Event.HANGUP)
 
    def on_dtmf(self, channel, event):
        digit = event.get('digit')

        self.cleanup()
        self.call.state_machine.change_state(digit)


