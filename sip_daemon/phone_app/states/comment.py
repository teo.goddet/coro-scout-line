import threading, uuid
from phone_app.event import Event
from base import BaseState
from messages import Message

from config import Config as config


class State(BaseState):
    state_name = "comment_before_end"
 
    def enter(self):
        self.hangup_event = self.call.channel.on_event('ChannelHangupRequest', self.on_hangup)

        self.call.play("DIVERS")
        #merci beacoup, vous pouvez laissez un e precision avant de raccrocher

        self.sound_event = self.call.channel.on_event('ChannelTalkingStarted',
                                                     self.on_talk_start)

        self.timer = threading.Timer(240, self.finish()) 
        self.timer.start()    

    def finish(self):
        self.cleanup()
        self.call.state_machine.change_state(Event.STATE_END)

    def on_hangup(self, channel, event):
        self.cleanup()
        self.call.state_machine.change_state(Event.HANGUP)

    def on_talk_start(self):
        self.silence_event = self.call.channel.on_event('ChannelTalkingFinished',
                                                     self.on_talk_finish)

    def on_talk_stop(self):
        self.finish()