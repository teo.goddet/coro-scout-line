from phone_app.event import Event

class State(object):
    state_name = "ending"
 
    def __init__(self, call):
        self.call = call
        self.hangup_event = None
        self.silence_event = None
        self.sound_event = None
        self.timer = None
 
    def enter(self):
        self.hangup_event = self.call.channel.on_event('ChannelHangupRequest', self.on_hangup)
        self.call.channel.hangup()

    def on_hangup(self, channel, event):
        self.cleanup()
        self.call.state_machine.change_state(Event.HANGUP)