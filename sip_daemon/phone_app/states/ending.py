import threading, uuid
from phone_app.event import Event
from base import BaseState
class State(BaseState):
    state_name = "ending"
 
    def enter(self):
        self.hangup_event = self.call.channel.on_event('ChannelHangupRequest', self.on_hangup)

        self.call.play("BCPG")
        #les scouts de l'ouest vous souhaite une bonn journee

        self.timer = threading.Timer(20, self.call.channel.hangup()) 
        self.timer.start()     

    def on_hangup(self, channel, event):
        self.cleanup()
        self.call.state_machine.change_state(Event.HANGUP)

    	        
