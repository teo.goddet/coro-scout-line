import threading, uuid, os
from phone_app.event import Event
from base import BaseState
from messages import Message

class State(BaseState):
    state_name = "welcome"
    #this state launch the welcome message and try to get as much as possible infos on the persobn
    def __init__(self, call):
        super(State, self).__init__(call)
        self.restart_count = 0

    def enter(self):
        print "Entering welcome state"
        self.hangup_event = self.call.channel.on_event('ChannelHangupRequest',
                self.on_hangup)
        self.dtmf_event = self.call.channel.on_event('ChannelDtmfReceived',
                                                     self.on_dtmf)
        self.call.play("BIENVENUE")

        self.timer = threading.Timer(120, self.restart) 
        self.timer.start()  
    def cleanup(self):
        print "Cleaning up event handlers"
        try:
            self.call.playback.stop()
        except Exception as error:
            print("error : ", error)

        if not self.timer == None:
            self.timer.cancel()   

        if not self.dtmf_event == None:
            self.dtmf_event.close()
        if not self.hangup_event == None:
            self.hangup_event.close()
        if not self.sound_event == None:
            self.sound_event.close()
 

    def on_dtmf(self, channel, event):
        digit = event.get('digit')
        if digit == "1":
            self.call.requete.categorie = "course"
        elif digit == "2":
            self.call.requete.categorie = "pharmacie" 
        elif digit == "3":
            self.call.requete.categorie = "autre"    
        elif digit == "5":
            self.call.requete = None    
        else: 
           self.restart()
           return;

        self.cleanup()
        self.call.state_machine.change_state(digit)

    def restart(self):
        try:
            self.call.playback.stop()
        except Exception as error:
            print("error : ", error)
        self.call.play("BIENVENUE")

        self.restart_count = self.restart_count + 1

        if(self.restart_count > 3):
            self.cleanup()
            self.call.state_machine.change_state(Event.TO_MANY_RESTART)

        self.timer = threading.Timer(120, self.restart) 
        self.timer.start()    