import requests, json, os, jsonpickle, copy, time
from config import Config as config


class State(object):
    state_name = "hungup"
 
    def __init__(self, call):
        self.call = call
 
    def enter(self):
        #record in db 
        if not self.call.requete == None:
            try:
                self.call.record.stop()
            except: 
                pass
            time.sleep(4)
            headers = {'X-Api-Key': config.API_KEY, 'Content-Type': 'application/json'}
            cp = copy.deepcopy(self.call.requete)
            cp.enregistrement = None
            body = {'requete': cp}
            try:
                r = requests.post(config.API_BASE_URL+"/requete/", headers=headers, data=jsonpickle.encode(body, unpicklable=False))
            except Exception as e:
                print e
                print "error in requete upload : "+str(vars(self.call.requete))
                return  
            if (200 <= r.status_code and r.status_code < 300):
                id = r.json()['requete']['_id']
                audio = {'record': open( os.path.join(config.SOUND_RECORD_DIR,self.call.requete.enregistrement.ref+config.RECORD_EXTENSION), 'rb')}
                try:
                    headers = {'X-Api-Key': config.API_KEY}
                    r = requests.post(config.API_BASE_URL+"/requete/"+id+"/audio/uploads", headers=headers, files=audio)
                except Exception as e:
                    print e
                    print "error in record upload : "+str(vars(self.call.requete))
                    return
                if (200 <= r.status_code and r.status_code < 300):
                    print "Succesfully added a requete"
                    return
            print "error in requete upload : "+str(self.call.requete)
            print "response: " + str(vars(r))
            print jsonpickle.encode(self.call.requete, unpicklable=False)

        else:
            print "this call wasn't a requete"
