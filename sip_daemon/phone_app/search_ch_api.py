from .models import Addresse
import requests, feedparser, re

def InfosFromNumber(number, entry=0):
    #do stuff to retrieve the address
    try:
        r = requests.get('https://tel.search.ch/api/', params={'was': number})
    except Exception as error:
        print ("ERROR with search.ch api : ", error)
    	return None, None
    	
    if (r.status_code != 200):
    	   	print ("ERROR with search.ch api : ",r.content)
    		return None, None
 
    feed = feedparser.parse(r.content)
 
    if (len(feed.entries) < 1):
        	print "No result in search.ch for this number : "+number
     		return None, None
     
    infos = feed.entries[entry].content[0].value.split('\n')
 
    cp_split = re.search('([0-9]{4})(.*)', infos[2])
 
    try: 
        cp = cp_split.group(1);
    except Exception as error:
        print ("Error processing infos : ", infos, error)
        cp = "Unknow"
 
    try:
        ville = cp_split.group(2).strip()
    except Exception as error:
        print ("Error processing infos : ", infos, error)
        ville = "Unknow"
 
    return (infos[0], Addresse(infos[1], ville, cp))
