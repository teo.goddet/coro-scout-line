from google.cloud import texttospeech
from config import Config as config


def tts(text, filename):
	# Instantiates a client
	client = texttospeech.TextToSpeechClient()
	
	# Set the text input to be synthesized
	synthesis_input = texttospeech.types.SynthesisInput(text=text)
	
	# Build the voice request, select the language code ("en-US") and the ssml
	# voice gender ("neutral")
	voice = texttospeech.types.VoiceSelectionParams(
	    language_code=config.LANG,
	    name='fr-FR-Wavenet-D')
	
	# Select the type of audio file you want returned
	audio_config = texttospeech.types.AudioConfig(
	    audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16,
	    pitch=2.8,
	    speaking_rate=0.87)
	
	# Perform the text-to-speech request on the text input with the selected
	# voice parameters and audio file type
	response = client.synthesize_speech(synthesis_input, voice, audio_config)
	
	# The response's audio_content is binary.
	with open(filename, 'wb') as out:
	    # Write the response to the output file.
	    out.write(response.audio_content)

def ssml(text, filename):
	# Instantiates a client
	client = texttospeech.TextToSpeechClient()
	
	# Set the text input to be synthesized
	synthesis_input = texttospeech.types.SynthesisInput(ssml=text)
	
	# Build the voice request, select the language code ("en-US") and the ssml
	# voice gender ("neutral")
	voice = texttospeech.types.VoiceSelectionParams(
	    language_code='fr-FR',
	    ssml_gender=texttospeech.enums.SsmlVoiceGender.MALE)
	
	# Select the type of audio file you want returned
	audio_config = texttospeech.types.AudioConfig(
	    audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16)
	
	# Perform the text-to-speech request on the text input with the selected
	# voice parameters and audio file type
	response = client.synthesize_speech(synthesis_input, voice, audio_config)
	
	# The response's audio_content is binary.
	with open(filename, 'wb') as out:
	    # Write the response to the output file.
	    out.write(response.audio_content)
