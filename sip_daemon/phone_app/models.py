# coding: utf8

import enum
from datetime import datetime


class User():
    def __init__(self, email, first_name, last_name, telephone):
        self.email = email
        self.first_name = first_name
        self.last_name = last_name
        self.telephone = telephone

#    def __repr__(self):
 #       return '<User {}>'.format(self.username)

class CashTransferRequete():
    def __init__(self, personne = None, requete = None, status='created'):
        self.personne = personne
        self.requete = requete
        self.status = status

  #  def __repr__(self):
   #     return str(self.id) + " / " + self.status

class Addresse():  
    def __init__(self, rue = "", commune = "", code_postal = ""):
        self.rue = rue
        self.commune = commune
        self.code_postal = code_postal

    #def __repr__(self):
     #   return self.rue + " ( " + self.code_postal + " , "  + self.commune + " ) "

class Enregistrement():
    def __init__(self, ref = ""):
        self.ref = ref

 #   def __repr__(self):
  #      return self.ref

    def toJSON(self):
        return self.__dict__

class RequeteStatut(enum.Enum):
    created = "created"
    in_progress = "in_progress"
    terminated = "terminated"


class RequeteCategorie(enum.Enum):
    course = "course"
    pharmacie = "pharmacie"
    autre = "autre"

class Requete():
    def __init__(self, personne = None, status='created', categorie = "autre"):
        self.personne = personne
        self.status = status
        self.categorie = categorie
        self.enregistrement = None

   # def __repr__(self):
    #    return "{} / {}".format(str(self.id), self.categorie)

class Personne():
    def __init__(self, numero_telephone = "", nom = "", addresse = None):
        self.telephone = numero_telephone
        self.addresse = addresse
        self.nom = nom

 #   def __repr__(self):
#        return self.nom