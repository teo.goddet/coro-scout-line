# coro-scout-line

This project aims to provide a tools to help ederly and needing people help with their daily life during the coronavirus epidemic.

Ce projet a pour but d'apporter une aide au personnes agées ou dans le besoin avec leur vie quotiedienne (achats, paiements, ....) durant l'épidémie de coronavirus. 

Le problème majeur des solutions actuelle est leur faible accesibilité par les personnes ne disposant pas d'accès a internet et a l'informatique en générale.

La solution doit donc être utilisable par téléphone. 

Dans la pluspart des cas, le recours a une hotline est complexe, l'idée est donc de fournir un robot vocal et d'assigner les enregistrement a des bénévoles. 

Le projet comporte donc plusieurs volet : 

 - robot vocal SIP (asterisk) (avec enregistrement et webhook vers une api de la web app)
    basé sur asterisk, un deamon python utilisant SQLAlchemy et ARI permettra de sequencer le robot vocal

 - webapp : dispatching et suivi des enregistrements, possibilité d'uploads les quittances et remboursement par ibaK
    utilise FLASk


STRUCTURE de donné 

requete: numero_telephone, nom_personne, addresse, enregistrement, utilisateur_en_charge, status
rbmt: photo_tickets, total, linked_request, status //TODO
addresse: champ
enregistrement: url, location
utilisateur: Flask User + num_telephone + IBAN


statut: enum { 0_non_attribue, 1_attribue, 2_fait_attente_rbmt, 2_fait_sans_rbmt, 3_rbmt, 4_fini } 